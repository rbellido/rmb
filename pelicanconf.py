#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Ronald Bellido'
SITENAME = u'Ronald Bellido'
SITEURL = 'http://rmb.name'

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Navigation links
MENUITEMS = (('/Blog', 'http://rmb.name/blog'),
        ('/Projects', 'http://rmb.name/projects'),
        ('/About', './other-pages/about'))


# Blogroll
LINKS =  (('Pelican', 'http://getpelican.com/'),
          ('Python.org', 'http://python.org/'),
          ('Jinja2', 'http://jinja.pocoo.org/'),
          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Specify the theme
THEME = "simply-better" #"themes/rmb-theme"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

